<?php

namespace Symbiont\Support\ForwardCall;

use Symbiont\Support\ForwardCall\Contracts\ForwardsCalls;
use Symbiont\Support\ForwardCall\Exceptions\BadMethodForwardCallException;
use Symbiont\Support\ForwardCall\Exceptions\MissingForwardMethodsException;
use Symbiont\Support\ForwardCall\Exceptions\MissingForwardObjectException;
use Symbiont\Support\ForwardCall\Exceptions\WrongReturnTypeException;

trait ForwardsCall {

    public function __call($name, array $args = []) {
        return static::findForwardCallMethod($this, $name, $args);
    }

    public static function __callStatic($name, array $args = []): mixed {
        return static::findForwardCallMethod(static::class, $name, $args);
    }

    protected static function findForwardCallMethod(string|ForwardsCalls $from, string $name, array $args) {
        $forward_methods = static::filterForwardMethods(get_class_methods($from));

        foreach($forward_methods as $forward_method) {
            list($method, $property) = $forward_method;

            $methods = static::collectForwardMethods($from, $method);
            $object = static::collectForwardObject($from, $property);

            if(! is_object($object)) {
                throw new MissingForwardObjectException(gettype($object));
            }

            if(! is_array($methods)) {
                throw new MissingForwardMethodsException(gettype($methods));
            }

            // check if method is bent
            if (array_key_exists($name, $methods)) {
                $name = $methods[$name];
            }

            if (in_array($name, $methods) && is_callable([$object, $name])) {
                return call_user_func_array([$object, $name], $args);
            }
        }

        throw new BadMethodForwardCallException(static::generateClassSpecificness($from), $name);
    }

    protected static function filterForwardMethods(array $methods): array {
        return array_values(array_filter(array_map(function($method) {
            if( ($parts = preg_split('/(?=[A-Z])/', $method)) && $parts[0] === 'forward' ) {
                array_shift($parts);
                return [
                    $method,
                    implode('_', array_values(array_map(function($value) {
                        return strtolower($value);
                    }, $parts)))
                ];
            }

            return null;
        }, $methods)));
    }

    protected static function collectForwardObject(string|ForwardsCalls $from, string $property) {
        if(property_exists($from, $property)) {
            return (new \ReflectionProperty($from, $property))->getValue(static::generateClassSpecificness($from, false));
        }

        throw new \Exception(sprintf('Expected `%s` property not set', $property));
    }

    protected static function collectForwardMethods(string|ForwardsCalls $from, string $method) {
        return (new \ReflectionMethod($from, $method))->invoke(static::generateClassSpecificness($from, false));
    }

    protected static function generateClassSpecificness(string|ForwardsCalls $from, bool $static = true): string|ForwardsCalls {
        switch($static) {
            case true:
            default:
                return (is_object($from) ? $from::class : $from);
                break;
            case false:
                return (is_string($from) ? new $from : $from);
                break;
        }
    }

}