<?php

namespace Symbiont\Support\ForwardCall\Exceptions;

class WrongReturnTypeException extends \Exception {

    public function __construct(string $type) {
        parent::__construct(sprintf('Forward method should return an array, given %s', $type));
    }

}