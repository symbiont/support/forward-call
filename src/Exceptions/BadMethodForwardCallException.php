<?php

namespace Symbiont\Support\ForwardCall\Exceptions;

class BadMethodForwardCallException extends \BadMethodCallException {

    public function __construct(string $class, string $name) {
        parent::__construct(sprintf('Forward call to undefined method %s::%s', $class, $name));
    }

}