<?php

namespace Symbiont\Support\ForwardCall\Exceptions;

class MissingForwardObjectException extends \Exception {

    public function __construct(string $type) {
        parent::__construct(sprintf('Missing forward object, given type %s', $type));
    }

}