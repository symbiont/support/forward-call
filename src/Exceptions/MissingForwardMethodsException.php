<?php

namespace Symbiont\Support\ForwardCall\Exceptions;

class MissingForwardMethodsException extends \Exception {

    public function __construct(string $type) {
        parent::__construct(sprintf('Missing forward object methods, given type %s', $type));
    }

}