![release](https://gitlab.com/symbiont/support/forward-call/-/badges/release.svg?key_text=version) ![pipeline](https://gitlab.com/symbiont/support/forward-call/badges/master/pipeline.svg?key_text=test)

# Forward Call

!!! warning
    This package is `work in progress`!

!!! Note
    This documentation only documents the technical usage of this package with little to no text.

Simple event dispatcher.

## Installation

Requires at least PHP `8.2`

```bash
composer require symbiont/support-forward-call
```

---

## Testing

```php
composer test
```

## License

Released under MIT License