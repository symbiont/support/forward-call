# Usage

Minimal forward call as a trait using magic methods `__call` and `__callStatic`.

## Guide lines

 - Class implements the contract `Symbiont\Support\ForwardCall\Contracts\ForwardsCalls`
 - Class uses the trait `Symbiont\Support\ForwardCall\ForwardsCall`
 - Each property containing an object to be forwarded should implement a method in kebab style starting with `forward`
     - e.g. property called `$container` should have a method named `forwardContainer()`
 - The forward method should return an array with all method names to be forwarded
 - Method names can bend, e.g. `['retrieve' => 'get']`, calling `retrieve()` will call `get()` on the forwarded object.

## Basic setup

```php
<?php
use Symbiont\Support\ForwardCall\Contracts\ForwardsCalls;
use Symbiont\Support\ForwardCall\ForwardsCall;

/**
 * @method bind($abstract, $concrete)
 * @method make($abstract)
 * ...  
 */
class AwesomeClass implements ForwardsCalls {
    use ForwardsCall;
    
    protected object $container;
    
    public function forwardContainer() {
        return ['bind', 'make', 'get'];
    }
}

$awesome = new Awesome;
$awesome->get();
// the equivalent of (new Awesome)->container->get();
```

### Colliding method names

If method names of different objects to be forwarded collide, names can bend. E.g. for some unexplainable reason, a 
`Container` and a `Config` class share the same methods `get()` and `set()`.

```php
<?php
use Symbiont\Support\ForwardCall\Contracts\ForwardsCalls;
use Symbiont\Support\ForwardCall\ForwardsCall;
use Some\Container;
use Some\Config;

/**
 * -> $container
 * @method bind($abstract, $concrete)
 * @method make($abstract)
 * -> $config
 * @method set($key, $value)
 * @method get($key)
 * 
 */
class AwesomeClass implements ForwardsCalls {
    use ForwardsCall;
    
    protected object $container;
    protected object $config;
    
    public function __construct() {
        $this->container = new Container;
        $this->config = new Config;
    }
    
    public function forwardContainer() {
        return [
            'bind' => 'set',
            'make' => 'get'
        ];
    }
    
    public function forwardConfig() {
        return ['get', 'set'];
    }
}

$awesome = new Awesome;
$awesome->bind($abstract, $concrete);
// $awesome->array->set($abstract, $concrete);
$awesome->get($key);
// $awesome->array->get($key);
```