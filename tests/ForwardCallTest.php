<?php

namespace Symbiont\Support\ForwardCall\Tests\unit;

use PHPUnit\Framework\TestCase;
use Symbiont\Support\ForwardCall\Contracts\ForwardsCalls;
use Symbiont\Support\ForwardCall\ForwardsCall;
use Symbiont\Support\ForwardCall\Exceptions\BadMethodForwardCallException;

final class ForwardCallTest extends TestCase {

    protected object $caller;

    public static string $message = 'forward from test';
    public static string $message_static = 'forward from static tester';

    protected function setUp(): void {
        $this->caller = new class() implements ForwardsCalls {
            use ForwardsCall;

            protected ?object $container = null;
            protected static ?object $static_container = null;

            public function __construct() {
                $this->container = new class {

                    public function test() {
                        return ForwardCallTest::$message;
                    }

                    public static function tester() {
                        return ForwardCallTest::$message_static;
                    }

                };
            }

            protected static function initialize() {
                static::$static_container = new class {

                    public function testing() {
                        return ForwardCallTest::$message;
                    }

                };
            }

            public function getForwardMethods(array $methods) {
                return $this::filterForwardMethods($methods);
            }

            public function forwardContainer(): array {
                return [
                    'test', 'tester'
                ];
            }

            public function forwardStaticContainer(): array {
                static::initialize();

                return [
                    'testing'
                ];
            }
        };
    }

    public function testFilterForwardObjects() {
        $expected = [['forwardThisMethod', 'this_method'], ['forwardObject', 'object']];
        $methods = $this->caller->getForwardMethods(['forwardThisMethod', 'forwardsNotThisMethod', 'forwardObject']);
        $this->assertEquals($expected, $methods);
    }

    public function testForward() {
        $this->assertEquals(ForwardCallTest::$message, $this->caller->test());
        $this->assertEquals(ForwardCallTest::$message_static, $this->caller::tester());
    }

    public function testStaticForward() {
        $this->assertEquals(ForwardCallTest::$message, $this->caller->testing());
    }

    public function testUndefinedForward() {
        $this->expectException(BadMethodForwardCallException::class);
        $this->caller->doesnotexist();
    }

    public function testUndefinedStaticForward() {
        $this->expectException(BadMethodForwardCallException::class);
        $this->caller::doesnotexist();
    }

}