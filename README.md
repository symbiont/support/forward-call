# Symbiont / Forward Call

Minimal forward call trait using magic methods `__call` and `__callStatic`.

This package is work in progress!

## Requirements

- `php 8.2`

## Installation

```bash
composer require symbiont/support-forward-call
```

## Usage

Simple example of using `ForwardsCall`.

```php

use Symbiont\Support\ForwardCall\Contracts\ForwardsCalls;
use Symbiont\Support\ForwardCall\ForwardsCall;

use Some\Container;
use Some\Dispatcher;
use Some\SomethingElse;

/**
 * @method methods
 * @method to
 * @method ..  
 */
class Awesome implements ForwardsCalls {

    use Forwardscall;

    protected object $container;
    protected object $dispatcher;
 
    public function __construct() {
        $this->container = new Container;
        $this->dispatcher = new Dispatcher;
        $this->something_else = new SomethingElse;
    }
 
    protected function forwardContainer(): array {
        return [
            'methods', 'to', 'be', 
            'forwarded' // static
        ];
    }
 
    protected function forwardDispatcher(): array {
        return [
            'method' => 'that',
            'can' => 'beBend',
            'mixed', 'with', 'others'
        ]
    }
    
    // converts to property `something_else`
    protected function forwardSomethingElse(): array {
        return [
           // .. methods
        ]       
    }
}

$object = new Awesome;
$object->methods();
// call $this->container->methods()
$object->can();
// calls $this->dispatcher->beBend()
$object::forwarded(); 
// calls $this->container::forwarded()
```

## Documentation

This documentation only documents the technical usage of this package with little to no text.

- Documentation: [https://symbiont.gitlab.io/support/forward-call](https://symbiont.gitlab.io/support/forward-call)
- Gitlab: [https://gitlab.com/symbiont/support/forward-call](https://gitlab.com/symbiont/support/forward-call)

## Tests

```bash
composer test
```

---

## License

[MIT license](LICENSE.MD)